#!/bin/bash

RUN_CMD="/usr/bin/python3 /home/pi/stargazy/cron/scripts/shutdown-and-restart-button.py"
RUN_NAME="shutdown listener"

/home/pi/stargazy/cron/utils/background_script.sh "${RUN_CMD}" "${RUN_NAME}"
