#Stargazy
import logging
from logging.handlers import RotatingFileHandler
import datetime
import glob
import time
import statistics

#LED
import RPi.GPIO as GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(36, GPIO.OUT, initial=GPIO.LOW) #GPIO16 on raspi zero w
def blink_LED():
  GPIO.output(36, GPIO.HIGH)
  time.sleep(0.1)
  GPIO.output(36, GPIO.LOW)

#ADC
import Adafruit_ADS1x15

#server
from flask import Flask
from flask import Response
from flask import request
app = Flask(__name__)

#config
import configparser
import os
configfilepath = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini')
config = configparser.ConfigParser()
config.read(configfilepath)

# store config in variables for later use
offset = config['offset']
sensors = []
for i in range(4):
    sensors.append( config['sensors'].get(str(i), 'default') )

@app.route("/")
def index():
  return "Stargazy"

@app.route("/config")
def config():
  result = "<h1>Config</h1>"
  result += "<p><b>Offsets</b>: <ul>"
  for key in offset:
      result += "<li>"+key+" = "+offset[key]+"</li>"
  result += "</ul></p>"
  result += "<p><b>Sensors</b>: <ul>"
  for sensor in sensors:
      result += "<li>"+sensor+"</li>"
  result += "</ul></p>"
  return result

@app.route("/test")
def test():
  now = datetime.datetime.now()
  return 'hour %d' % now.hour + '\n' + 'minute %d' % now.minute + '\n' + 'second %d' % now.second + '\n'

@app.route("/temperature")
#supports DS18B20+ sensors over 1Wire; uses +3V3 and GPIO4
def temperature():
  result = ''
  for name in glob.glob('/sys/bus/w1/devices/28*'):
    sensor = name[len('/sys/bus/w1/devices/')+3:]
    f = open(name+'/w1_slave', 'r')
    lines = f.readlines()
    f.close()
    yes = lines[0].strip()[-3:]
    temp = "N/A"
    if yes == 'YES':
        equals_pos = lines[1].find('t=')
        temp = lines[1][equals_pos+2:]
        temp_c = float(temp) / 1000.0
        if sensor in offset:
          temp_c = temp_c + offset.getfloat(sensor)
    result = result + 'temperature{sensorid="' + sensor + '",unit="C"}} {0:.2f}\n'.format(temp_c)
  blink_LED()
  return Response(result, mimetype='text/plain')

@app.route("/adc")
#uses the AdaFruit library for ADS1x15 chips; 4 analogue channel mode over I2C1
def adc():
  #ADC sensors needs to be configured here in order of ADC channels 0->3
  #
  #light - for a LDR against a 48kOhm resistor
  #air - MPX5010DP pressure sensor in positive mode; to monitor an air pump
  #hall - A1301 hall effect sensor; to monitor a magnetic impeller
  #flow - YF-S201C flow sensor
  #default - returns mean and stdev
  #

  adc = Adafruit_ADS1x15.ADS1015()
  adc.start_adc(0, gain=1)
  #gain values: 2/3 (+-6.144V), 1 (+-4.096V), 2 (+-2.048V), 4 (+-1.024V), 8 (+-0.512V), 16 (+-0.256)
  #adc values will range from -2048 to 2047 across this range
  result = ''
  vals = [list(), list(), list(), list()]
  times = []
  for i in range(4):
    start = time.time()
    if sensors[i] == "light":
      for t in range(30):
        val = adc.read_adc(i, gain=2/3)
        vals[i].append(val / 2048 * 6.144)
    elif sensors[i] == "air":
      for t in range(30):
        val = adc.read_adc(i, gain=2)
        vals[i].append(val / 2048 * 2.048)
    elif sensors[i] == "hall":
      for t in range(200):
        val = adc.read_adc(i, gain=1)
        if val>600 and val<1800: #returns can be glitchy
          vals[i].append(val / 2048 * 2.048)
    elif sensors[i] == "flow":
      for t in range(200):
        val = adc.read_adc(i, gain=2/3)
        vals[i].append(val)
    else:
      for t in range(10):
        val = adc.read_adc(i, gain=1)
        vals[i].append(val / 2048 * 4.096)
        time.sleep(0.010)
    times.append(time.time()-start)
  adc.stop_adc()

  for i in range(4):
    avg = statistics.mean(vals[i])
    stdev = statistics.stdev(vals[i]) #only make sense for discrete samples
    if sensors[i] == "light":
      result = result + 'adc{{type="light",adcid="{0}",unit="V"}} {1:.3f}\n'.format(i, avg)
      result = result + 'adc{{type="light_stdev",adcid="{0}",unit="%"}} {1:.1f}\n'.format(i, stdev / avg * 100.0)
      result = result + 'adc{{type="light_time",adcid="{0}",unit="s"}} {1:.3f}\n'.format(i, times[i])
    elif sensors[i] == "air":
      #Vout = Vs*(0.09*P+0.04) according to spec
      #on a diaphragm pump stdev will likely be high due to ripple
      press = ((avg / 5.0) - 0.04) / 0.09 * 10.0
      result = result + 'adc{{type="air",adcid="{0}",unit="hPa"}} {1:.1f}\n'.format(i, press)
      result = result + 'adc{{type="air_stdev",adcid="{0}",unit="%"}} {1:.1f}\n'.format(i, stdev / avg * 100.0)
      result = result + 'adc{{type="air_time",adcid="{0}",unit="s"}} {1:.3f}\n'.format(i, times[i])
    elif sensors[i] == "hall":
      result = result + 'adc{type="hall",adcid="' + str(i) + '",unit="mV"} '
      #for val in vals[i]:
        #result = result + str(val) + " "
      result = result + '{0:.1f}'.format((max(vals[i]) - min(vals[i])) * 1000.0) + '\n'
      result = result + 'adc{{type="hall_time",adcid="{0}",unit="s"}} {1:.3f}\n'.format(i, times[i])
    elif sensors[i] == "flow":
      nhigh = 0
      ishigh = False
      threshold = 700
      for val in vals[i]:
        #result = result + " " + str(val) 
        if not ishigh and val > threshold:
          ishigh = True
          nhigh += 1
        if ishigh and val < threshold:
          ishigh = False
      if nhigh == 1:
        nhigh = 0 #fix state where rotor stops under the sensor
      flowHz = nhigh / times[i]
      flowVol = flowHz / 5.0 #F(Hz)=5*W(L/min) according to spec
      result = result + 'adc{{type="flow",adcid="{0}",unit="L/min"}} {1:.2f}\n'.format(i, flowVol)
      result = result + 'adc{{type="flow_count",adcid="{0}"}} {1}\n'.format(i, nhigh)
      result = result + 'adc{{type="flow_time",adcid="{0}",unit="s"}} {1:.3f}\n'.format(i, times[i])
    else:
      result = result + 'adc{{type="unknown_mean",adcid="{0}"}} {1:.3f}\n'.format(i, avg)
      result = result + 'adc{{type="unknown_stdev",adcid="{0}",unit="%"}} {1:.3f}\n'.format(i, stdev / avg * 100.0)
      result = result + 'adc{{type="unknown_time",adcid="{0}",unit="s"}} {1:.3f}\n'.format(i, times[i])
  blink_LED()
  return Response(result, mimetype='text/plain')

@app.route("/hall")
#helper endpoint to aid finding good location for the hall sensor
def hall():
  i = request.args.get('id', default = -1, type = int) #adc channel needed
  if i > -1:
    adc = Adafruit_ADS1x15.ADS1015()
    adc.start_adc(0, gain=1)
    result = '<html><head><meta http-equiv="refresh" content="4" /></head><body>Hall sensor: '
    vals = []
    for t in range(200):
      val = adc.read_adc(i, gain=1)
      if val>600 and val<1800: #returns can be glitchy
        vals.append(val / 2048 * 2.048)
    if len(vals) > 0:
      result = result + '<b>{0:.3f}</b> delta V'.format(max(vals)-min(vals))
    else:
      result = result + 'no data, wrong channel?'
    return result + '</body></html>'
  else:
    return "Need the ADC channel, use: /hall?id=0"

if __name__ == "__main__":
  handler = RotatingFileHandler('error.log', maxBytes=1024*1024*10, backupCount=1)
  app.logger.setLevel(logging.ERROR)
  app.logger.addHandler(handler)
  #app.run(host='0.0.0.0', debug=True, threaded=True)
  app.run(host='0.0.0.0', threaded=True)

