#!/bin/bash

RUN_CMD=$1
RUN_NAME=$2

# cmd must be supplied
[ -z "$RUN_CMD" ] && exit 1

echo "Check if ${RUN_CMD} (${RUN_NAME}) is running"
FOUND=`pgrep -f "^$RUN_CMD"`

echo "found: $FOUND"
if [[ -z $FOUND ]]
then
    echo "Starting $RUN_NAME: $RUN_CMD"
    echo "Starting $RUN_NAME: $(date)" >> /home/pi/stargazy/cron/cron.log
    $RUN_CMD &
fi
