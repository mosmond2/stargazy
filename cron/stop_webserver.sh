#!/bin/bash

RUN_CMD="/usr/bin/python3 /home/pi/stargazy/webserver/server.py 2>>/home/pi/stargazy/webserver/error.log"

pkill -f "^$RUN_CMD"
