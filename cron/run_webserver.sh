#!/bin/bash

RUN_CMD="/usr/bin/python3 /home/pi/stargazy/webserver/server.py 2>>/home/pi/stargazy/webserver/error.log"
RUN_NAME="webserver"

/home/pi/stargazy/cron/utils/background_script.sh "${RUN_CMD}" "${RUN_NAME}"
