#!/usr/bin/env python3
from gpiozero import Button, LED
from signal import pause
import os, sys

offGPIO = 21
restart_time = 1 
shutdown_time = 3 
min_time = 0.5
# http://gpiozero.readthedocs.io/en/stable/api_output.html#led
ledGPIO = 16 # GPIO16 http://gpiozero.readthedocs.io/en/stable/recipes.html#pin-numbering 

pressed_time = None

# the function called to shut down the RPI
def restart():
    print('restart')
    os.system("sudo shutdown -r now")

def shutdown():
    print('shutdown')
    os.system("sudo poweroff")

def when_held(b):
    global pressed_time 
    pressed_time = b.pressed_time
    print('when_held pressed for '+str(pressed_time), flush=True)
    if pressed_time is not None:
        if pressed_time > shutdown_time:
            shutdown()
        elif pressed_time > restart_time:
            led.on()
   

def when_pressed(b):
    global pressed_time
    pressed_time = b.pressed_time
    print('when_pressed blink '+str(pressed_time), flush=True)
    led.blink(on_time=0.1, off_time=0.1, n=3)

def when_released():
    led.off()
    print('when_released pressed for '+str(pressed_time), flush=True)
    if pressed_time is not None:
        if pressed_time > shutdown_time: 
            shutdown()
        elif pressed_time > restart_time:
            restart()

led = LED(ledGPIO)

btn = Button(offGPIO, hold_time=min_time, hold_repeat=True)
btn.when_held = when_held
btn.when_pressed = when_pressed
btn.when_released = when_released
print('watching ... ', flush=True)
pause()    # handle the button presses in the background
